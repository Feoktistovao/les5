package ru.feoktistov.tm;

import ru.feoktistov.tm.controller.ProjectController;
import ru.feoktistov.tm.controller.SystemController;
import ru.feoktistov.tm.controller.TaskController;

import ru.feoktistov.tm.endpoint.ProjectEndpoint;
import ru.feoktistov.tm.endpoint.TaskEndpoint;
import ru.feoktistov.tm.repository.ProjectRepository;
import ru.feoktistov.tm.repository.TaskRepository;
import ru.feoktistov.tm.service.DomainService;
import ru.feoktistov.tm.service.ProjectService;
import ru.feoktistov.tm.service.TaskService;


import javax.xml.ws.Endpoint;
import java.util.Scanner;

import static ru.feoktistov.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskService taskService = new TaskService(taskRepository);

    private final TaskController taskController = new TaskController(taskService);

    private final DomainService domainService = new DomainService(projectService, taskService);



    private final SystemController systemController = new SystemController(domainService);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService);

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService);

    {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        taskRepository.create("TEST TASK 1");
        taskRepository.create("TEST TASK 2");

    }

    public static void main(final String[] args) throws Exception{



        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        Endpoint.publish("http://0.0.0.0:8080/TaskEndpont?wsdl", app.taskEndpoint);
        Endpoint.publish("http://0.0.0.0:8080/ProjectEndpoint?wsdl", app.projectEndpoint);
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args) throws Exception{
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) throws Exception{
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return  systemController.displayAbout();
            case HELP: return  systemController.displayHelp();
            case EXIT: return  systemController.displayExit();

            case DATA_EXPORT_BASE64: return systemController.exportBASE64();
            case DATA_EXPORT_JSON: return systemController.exportJSON();
            case DATA_EXPORT_XML: return systemController.exportXML();

            case DATA_IMPORT_BASE64: return systemController.importBASE64();
            case DATA_IMPORT_JSON: return systemController.importJSON();
            case DATA_IMPORT_XML: return systemController.importXML();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case TASK_LIST: return taskController.listTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();

            default: return systemController.displayError();
        }
    }


}
