package ru.feoktistov.tm.endpoint;

import ru.feoktistov.tm.entity.Project;
import ru.feoktistov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint {

    private ProjectService projectService;

    public ProjectEndpoint(ProjectService projectService) {
        this.projectService = projectService;
    }



    @WebMethod
    public Project createProject(@WebParam(name = "ProjectName") String name) {
        return projectService.create(name);
    }

    @WebMethod
    public void clear() {
        projectService.clear();
    }
    @WebMethod
    public Project findByIndex(int index) {
        return projectService.findByIndex(index);
    }
    @WebMethod
    public Project findByName(String name) {
        return projectService.findByName(name);
    }
    @WebMethod
    public Project findById(Long id) {
        return projectService.findById(id);
    }
    @WebMethod
    public Project removeByIndex(int index) {
        return projectService.removeByIndex(index);
    }
    @WebMethod
    public Project removeById(Long id) {
        return projectService.removeById(id);
    }
    @WebMethod
    public Project removeByName(String name) {
        return projectService.removeByName(name);
    }
    @WebMethod
    public List<Project> findAll() {
        return projectService.findAll();
    }
}
