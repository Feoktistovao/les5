package ru.feoktistov.tm.endpoint;

import ru.feoktistov.tm.entity.Task;
import ru.feoktistov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint {

    private TaskService taskService;

    public TaskEndpoint() {

    }


    public TaskEndpoint(TaskService taskService) {
        this.taskService = taskService;
    }


    @WebMethod
    public Task create(String name) {
        return taskService.create(name);
    }
    @WebMethod
    public Task findByIndex(int index) {
        return taskService.findByIndex(index);
    }
    @WebMethod
    public Task findByName(String name) {
        return taskService.findByName(name);
    }
    @WebMethod
    public Task removeById(Long id) {
        return taskService.removeById(id);
    }
    @WebMethod
    public Task removeByName(String name) {
        return taskService.removeByName(name);
    }
    @WebMethod
    public Task findById(Long id) {
        return taskService.findById(id);
    }
    @WebMethod
    public void clear() {
        taskService.clear();
    }
    @WebMethod
    public List<Task> findAll() {
        return taskService.findAll();
    }
}
