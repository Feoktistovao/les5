package ru.feoktistov.tm.service;

import org.junit.Assert;
import org.junit.Test;
import ru.feoktistov.tm.repository.ProjectRepository;

public class ProjectServiceTest {
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    @Test
    public void testProject(){
        String name = "Test project1";
        Assert.assertNotNull(projectService.create(name));
        Assert.assertNotNull(projectService.findByName(name));
        Assert.assertEquals(name,projectService.findByName(name).getName());
        Assert.assertNotNull(projectService.removeByName(name));
        Assert.assertNull(projectService.findByName(name));
    }
}
