package ru.feoktistov.tm.service;

import org.junit.Assert;
import org.junit.Test;

import ru.feoktistov.tm.repository.TaskRepository;


public class TaskServiceTest {


    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    @Test
    public void testTask(){
        String name = "Test task1";
        Assert.assertNotNull(taskService.create(name));
        Assert.assertNotNull(taskService.findByName(name));
        Assert.assertEquals(name,taskService.findByName(name).getName());
        Assert.assertNotNull(taskService.removeByName(name));
        Assert.assertNull(taskService.findByName(name));
    }

}
